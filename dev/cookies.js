// // Set Options Here
// var options = {
// 	message: "This website uses necessary cookies to ensure you get the best experience on our website.",
// 	link: "/cookies/",
// 	colours: {
// 		background: "#252525",
// 		text: "#fff",
// 		button: "#fff",
// 	},
// 	sizes: {
// 		header: "1.4em",
// 		message: "1.1em",
// 		buttons: "1.1em"
// 	},
// 	essential_cookies: ['cookie_preference', ' cookie_preference', 'DYNSRV']
// };

// // Set Cookies Here
// function loadCookies() {
// 	ga('create', 'UA-39778143-1', 'auto');
// 	ga('send', 'pageview');
// }

// Do Not Edit Below This Line --------------------------------

// var options = {
// 	domain: ".dev.fever",
// 	essential_cookies: ['cookie_preference', ' cookie_preference', 'DYNSRV']
// };

// var essential = '';
// var domain = '';
var scripts = '';

var options = '';

function hasClass(element, cls) {
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}

function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/; domain="+options.domain+"; SameSite=None; Secure";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function cookiesAllowed(safe_mode) {
	if(getCookie('cookie_preference') == 'deny') return false;
	if(getCookie('cookie_preference') == 'allow') return true;
	if(safe_mode == false) return true;
	return false;
}
function deleteCookie(name) {   
    document.cookie = name+'=;Expires=Thu, 01 Jan 1970 00:00:01 GMT;';  
}
function deleteCookies() {

	if(getCookie('cookie_preference') != null) {
		uncheck();
	}

	//List of essential cookies - set as an empty array to delete everything - i.e. var essential = [];
	var essential = options.essential_cookies;
	// options.essential_cookies = ['cookie_preference', ' cookie_preference', 'DYNSRV'];
	
	//create array of cookies set
	var cookies = document.cookie.split(";");
	
	//loop through the cookies
	for (var i = 0; i < cookies.length; i++) {
		var cookie = cookies[i];
		
		//Get the cookie name
		var eqPos = cookie.indexOf("=");
		var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
		// Delete all cookies except those listed in essential
		if (essential.indexOf(name) == -1) {
			document.cookie = name +'=;expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/;domain='+options.domain+';';
		}
	}
}

function changeStatus(status) {
	console.log(status);
	setCookie('cookie_preference', status, '365');

	var notice = document.getElementById("cookie-notice");
	notice.classList.remove("cn-active");
	
	createSettings();

	if(status == 'deny') {
		deleteCookies();
	} else {
		loadCookies();
	}
}

function loadCookies() {
	check();
	if(typeof scripts == "function") {
		console.log('Cookies Loaded');
		scripts();
	}
}

function createNotice() {
	var cookieNotice = document.createElement('div');
	if(options.safe_mode == true) {
		button_text = 'Allow Cookies';
		close_button = '';
	} else {
		button_text = "OK, I'm fine with this";
		close_button = '<div class="cs-close" onclick="toggleNotice()" style="color:'+options.colours.text+'">&times;</div>';
	}
	if(options.colours.background == 'white') { options.colours.background = '#fff' }
	if(options.colours.text == 'dark') { options.colours.text = '#333' }
	if(options.colours.button == 'green') { options.colours.button = '#88c438' }
	

	cookieNotice.innerHTML = close_button+'<div class="cn-header" style="font-size:'+options.sizes.header+';color:'+options.colours.text+'">Cookies</div><div class="cn-body" style="font-size:'+options.sizes.message+';color:'+options.colours.text+'">'+options.message+'</div><div class="buttons"><div class="cn-button cn-primary" style="font-size:'+options.sizes.buttons+';background:'+options.colours.button+';color:'+options.colours.background+'" onClick=changeStatus("allow")>'+button_text+'</div><div class="cn-secondary" style="font-size:'+options.sizes.message+';color:'+options.colours.text+';" onClick=toggleSettings()>Change Settings</div></div>';
	cookieNotice.setAttribute('class', 'cookie-notice');
	cookieNotice.setAttribute('id', 'cookie-notice');
	cookieNotice.setAttribute('style', 'background:'+options.colours.background);
	document.body.appendChild(cookieNotice);
	cookieNotice.className = 'cookie-notice cn-active';
}

function createSettings() {
	settings_information_1 = '<strong>Essential cookies</strong> are required for you to use this website. Without these cookies services you have asked for cannot be provided.';
	settings_information_2 = '<strong>Non-essential cookies</strong> are used to collect information for making reports that help us improve the site. The cookies collect information in an anonymous form.';
	// Button
	var cookieSettingsBtn = document.createElement('div');
	cookieSettingsBtn.innerHTML = '<div class="cookie-settings-button" onclick="toggleSettings()" style="background:'+options.colours.background+';color:'+options.colours.text+'">Cookie Settings</div>';
	cookieSettingsBtn.setAttribute('id', 'cookie-settings-button');
	cookieSettingsBtn.setAttribute('class', 'cookie-settings-button-wrapper cs-hide');
	document.body.appendChild(cookieSettingsBtn);
	// Panel
	var cookieSettings = document.createElement('div');
	cookieSettings.innerHTML = '<div class="cs-close" onclick="toggleSettings()">&times;</div><div class="cs-header">Cookie Settings</div><div class="cs-body">'+settings_information_1+'</div><input type="checkbox" id="id-name--1" name="set-name" class="switch-input" disabled checked><label for="id-name--1" class="switch-label disabled">Essential cookies</label><div class="cs-body">'+settings_information_2+'</div><input type="checkbox" id="id-non-essential--2" onclick="changeSetting()" name="set-non-essential" class="switch-input"><label for="id-non-essential--2" class="switch-label">Non-essential cookies</label><div class="cs-body">See our <a href="'+options.link+'">Privacy Policy</a> for more details</div>';
	cookieSettings.setAttribute('id', 'cookie-settings');
	cookieSettings.setAttribute('class', 'cookie-settings');
	document.body.appendChild(cookieSettings);
	// // Overlay
	// var cookieSettingsOverlay = document.createElement('div');
	// cookieSettingsOverlay.setAttribute('id', 'cookie-overlay');
	// cookieSettingsOverlay.setAttribute('class', 'cookie-settings-overlay');
	// document.body.appendChild(cookieSettingsOverlay);
	// cookieSettingsOverlay.style.display = 'none';
	if ((window.innerHeight + window.pageYOffset + (window.pageYOffset / 3)) >= document.body.scrollHeight) {
		cookieSettingsBtn.setAttribute('class', 'cookie-settings-button-wrapper cs-show');
	}
	window.onscroll = function(ev) {
		if ((window.innerHeight + window.pageYOffset + (window.pageYOffset / 3)) >= document.body.scrollHeight) {
			cookieSettingsBtn.setAttribute('class', 'cookie-settings-button-wrapper cs-show');
		} else {
			cookieSettingsBtn.setAttribute('class', 'cookie-settings-button-wrapper cs-hide');
		}
	};

	console.log('Cookie Settings Loaded');
}

function changeSetting() {

	var checkBox = document.getElementById("id-non-essential--2");
  
	if (checkBox.checked == true) {
		console.log('Checked!');
		setCookie('cookie_preference', 'allow', '365');
		loadCookies();
	} else {
		console.log('Unchecked :(');
		setCookie('cookie_preference', 'deny', '365');
		deleteCookies();
	}
}

function check() {
    document.getElementById("id-non-essential--2").checked = true;
}

function uncheck() {
    document.getElementById("id-non-essential--2").checked = false;
}

function toggleNotice() {

	var notice = document.getElementById("cookie-notice");
	notice.classList.remove("cn-active");
	if(options.safe_mode == false) {
		changeStatus("allow");
	} 

}

function toggleSettings() {

	if(getCookie('cookie_preference') == null) {
		var notice = document.getElementById("cookie-notice");
		notice.classList.remove("cn-active");
		if(options.safe_mode == true) {
			changeStatus("deny");
		} else {
			changeStatus("allow");
		}
	}

	console.log('Cookie Settings');

	var settings = document.getElementById("cookie-settings");
	// var overlay = document.getElementById("cookie-overlay");
	
	if(hasClass(settings, 'cs-active')) {
		settings.className = 'cookie-settings';
		// overlay.style.display = 'none';
	} else {
		settings.className = 'cookie-settings cs-active';
		// overlay.style.display = 'block';
	}

}

function initCookies(userOptions, callback) {

	// Set User options as global options
	options = userOptions;

	// Set users callback as global cookie scripts
	scripts = callback;	

	if(options.safe_mode == undefined) {
		options.safe_mode = true;
	}

	if(!options.message) {
		if(options.safe_mode == true) {
			options.message = "We use necessary cookies to ensure you get the best experience on our website. All cookies collect information in an anonymous form.";
		} else {
			options.message = "We have placed cookies on your device to help make our site better. You can change this behaviour below, otherwise we will assume you are ok to continue.";
		}
	}
	if(!options.colours.background) {
		options.colours.background = '#333';
	}
	if(!options.colours.text) {
		options.colours.text = '#fff';
	}
	if(!options.colours.button) {
		options.colours.button = '#fff';
	}
	if(!options.sizes.header) {
		options.sizes.header = '1.4em';
	}
	if(!options.sizes.message) {
		options.sizes.message = '1.1em';
	}
	if(!options.sizes.buttons) {
		options.sizes.buttons = '1.1em';
	}

	console.log('Safe Mode: '+options.safe_mode);

	if(getCookie('cookie_preference') == null) {
		createNotice();
	}

	createSettings();

	if(cookiesAllowed(options.safe_mode)) {
		// Cookies allowed
		if(options.safe_mode == true) {
			console.log('Cookies Allowed');
		}
		loadCookies();
	} else {
		// Cookies disabled
		console.log('Cookies Disabled');
		deleteCookies();
	}

	// if(typeof callback == "function") {
		
	// 	function loadCookies() {
			
	// 		callback();
	// 	}
	// }
}

// window.addEventListener("load", function() {
// 	console.log('Inner Height: '+window.innerHeight);
// 	console.log('pageYOffset: '+window.pageYOffset);
// 	console.log('Offset: '+(window.pageYOffset / 4));
// 	var cookieButton = document.getElementById("cookie-settings-button");
// 	console.log(cookieSettingsBtn);
// 	window.onscroll = function(ev) {
// 		if ((window.innerHeight + window.pageYOffset + (window.pageYOffset / 4)) >= document.body.scrollHeight) {
// 			// alert("you're at the bottom of the page");
// 			console.log('Bottom!');
// 			cookieButton.setAttribute('class', 'cookie-settings-button cs-show');
// 		} else {
// 			cookieButton.setAttribute('class', 'cookie-settings-button cs-hide');
// 		}
// 	};
// });