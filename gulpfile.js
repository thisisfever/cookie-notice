//gulpfile.js

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var uglify = require('gulp-uglify');

//style paths
var sassFiles = './dev/*.scss',
	dest = './prod/',
	jsFiles = './dev/*.js'
	;

// Set the browser that you want to support
const AUTOPREFIXER_BROWSERS = [
	'ie >= 10',
	'ie_mob >= 10',
	'ff >= 30',
	'chrome >= 34',
	'safari >= 7',
	'opera >= 23',
	'ios >= 7',
	'android >= 4.4',
	'bb >= 10'
];

gulp.task('styles', function() {
    gulp.src(sassFiles)
        // .pipe(sourcemaps.init())
        .pipe(sass({
          outputStyle: 'compressed'
        }).on('error', sass.logError))
        // .pipe(sourcemaps.write('maps'))
        .pipe(autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
        .pipe(gulp.dest(dest));
});

gulp.task('scripts', function() {
	gulp.src(jsFiles)
	  // Minify the file
	  .pipe(uglify())
	  // Output
	  .pipe(gulp.dest(dest));
  });

gulp.task('watch',function() {
	gulp.watch(sassFiles,['styles']);
	gulp.watch(jsFiles,['scripts']);
});

gulp.task('default', ['styles']);