# Cookie Notice

## Introduction 

A simple, GDPR compliant cookie notice. 

- Only run cookies if user accepts
- Allows preference to be changed at any time via settings
- Settings Button automaically appears in bottom right once the user scrolls down the page.

## Getting started

Add the following scripts to your header file:

```html
<link rel="stylesheet" href="//files.thisisfever.co.uk/cookies.css" type="text/css" media="all" />
<script type="text/javascript" src="//files.thisisfever.co.uk/cookies.js"></script>
<script>
	window.addEventListener("load", function() {
		initCookies({
			domain: ".domain.co.uk",
			message: "This website uses necessary cookies to ensure you get the best experience on our website.",
			link: "/privacy-policy/",
			colours: {
				background: "#252525",
				text: "#fff",
				button: "#fff",
				safe_mode: false
			},
			sizes: {
				header: "1.4em",
				message: "1.1em",
				buttons: "1.1em"
			},
			essential_cookies: ['cookie_preference', ' cookie_preference', 'DYNSRV'],
		}, function() {
			// Add anything you wish to run once cookies have been accepted.

			// Standard Google Analytics
			ga('create', 'UA-xxxxxxxx-x', 'auto');
			ga('send', 'pageview');
			ga('set', 'anonymizeIp', true);

			// gtag Analytics
			window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-xxxxxxxx-x');
            gtag('config', 'UA-xxxxxxxx-x', { 'anonymize_ip': true });

			// Tag Manager
			if(typeof dataLayer != 'undefined') {
				dataLayer.push({
					'event': 'allow_cookies'
				});
			}

		});
	});
</script>
```

